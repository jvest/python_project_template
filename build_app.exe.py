## Compiles app.py to create a Windows executable (app.exe)
## Configuration is read from app.conf
## Modules must be installed in Python 2.7 in Wine
## Example (Installing the requests module):
##   wine cmd
##   c:\Python27\Scripts\easy_install requests

import ConfigParser, fileinput, re, subprocess, os

# Read and Set Configuration from Config File
Config = ConfigParser.ConfigParser()
Config.read('./app.conf')

def ConfigSectionMap(section):
    dict1 = {}
    options = Config.options(section)
    for option in options:
        try:
            dict1[option] = Config.get(section, option)
            if dict1[option] == -1:
                DebugPrint("skip: %s" % option)
        except:
            print("exception on %s!" % option)
            dict1[option] = None
    return dict1

# Debug Parameters
DEBUG = ConfigSectionMap('COMMON')['debug']

# Python Parameters
PYTHON_WIN = ConfigSectionMap('PYTHON')['python_win']
PYTHON_NX = ConfigSectionMap('PYTHON')['python_nx']
PYINSTALLER = ConfigSectionMap('PYTHON')['pyinstaller']
OUTPUT = ConfigSectionMap('PYTHON')['output']
APPSOURCE = ConfigSectionMap('PYTHON')['appsource']   

if os.name == 'nt':
    PYTHON = PYTHON_WIN
else: 
    PYTHON = PYTHON_NX

# Pyinstaller Commandline
command = PYTHON + " " + PYINSTALLER + " --onefile" + " --log-level=ERROR" + " --out=" + OUTPUT + " --console " + APPSOURCE

# Find and replace paramters in constants.py
## Optional dynamic build module

# Build app.py
a = subprocess.call(command, shell=True)
if a == 0:
    print "\n[+] Build Sucessful - (.exe located in " + OUTPUT + "/dist)\n"
else:
    print "\n[-] Build Failed\n"






